import com.google.protobuf.Descriptors.*
import com.google.protobuf.DynamicMessage
import com.google.protobuf.DynamicMessage.Builder
import com.google.protobuf.DynamicMessage.getDefaultInstance
import com.google.protobuf.util.JsonFormat
import java.io.File

fun main() {
    // Set the path to the input protocol buffer file
    val inputFile = File("path/to/input/file.proto")

    // Parse the input file into a list of message descriptors
    val fileDescriptor = FileDescriptor.buildFrom(inputFile.inputStream(), arrayOf())
    val messageDescriptors = fileDescriptor.messageTypes

    // Generate a Clojure namespace containing Spec definitions for each message
    val namespace = generateSpecNamespace(messageDescriptors)

    // Print the Clojure namespace to the console
    println(namespace)
}

fun generateSpecNamespace(messageDescriptors: List<Descriptor>): String {
    val namespace = StringBuilder()

    // Add the required namespace declaration and Spec import
    namespace.append("(ns my.specs\n")
            .append("  (:require [clojure.spec.alpha :as s]))\n\n")

    // Generate a Spec definition for each message
    for (messageDescriptor in messageDescriptors) {
        val specName = messageDescriptor.name
        val spec = generateMessageSpec(messageDescriptor)

        // Add the Spec definition to the namespace
        namespace.append("(s/def ::").append(specName).append(' ').append(spec).append(")\n\n")
    }

    return namespace.toString()
}

fun generateMessageSpec(messageDescriptor: Descriptor): String {
    val builder = getDefaultInstance(messageDescriptor).newBuilderForType()

    // Generate a Spec map for each field in the message
    for (fieldDescriptor in messageDescriptor.fields) {
        val fieldName = fieldDescriptor.name
        val fieldType = fieldDescriptor.javaType
        val fieldSpec = generateFieldSpec(fieldDescriptor)

        // Add the Spec map to the builder for the message
        builder.setField(fieldDescriptor, fieldSpec)
    }

    // Build the final Spec map for the message
    val messageSpec = builder.build()
    return messageSpec.toSpecString()
}

fun generateFieldSpec(fieldDescriptor: FieldDescriptor): Any {
    val fieldType = fieldDescriptor.javaType

    return when {
        fieldDescriptor.isMapField -> {
            // Generate a Spec map for the map field
            val keySpec = generateFieldSpec(fieldDescriptor.messageType.findFieldByNumber(1))
            val valueSpec = generateFieldSpec(fieldDescriptor.messageType.findFieldByNumber(2))
            s/mapOf(keySpec to valueSpec)
        }
        fieldDescriptor.isRepeated -> {
            // Generate a Spec map for the repeated field
            val elementSpec = generateFieldSpec(fieldDescriptor.messageType.findFieldByNumber(1))
            s/collOf(elementSpec)
        }
        else -> {
            // Generate a Spec map for the scalar field
            when (fieldType) {
                FieldDescriptor.JavaType.INT -> s/int
                FieldDescriptor.JavaType.LONG -> s/long
                FieldDescriptor.JavaType.FLOAT -> s/float
                FieldDescriptor.JavaType.DOUBLE -> s/double
                FieldDescriptor.JavaType.BOOLEAN -> s/bool
                FieldDescriptor.JavaType.STRING -> s/str
                FieldDescriptor.JavaType.BYTE_STRING -> s/bytes
                FieldDescriptor.JavaType.ENUM -> {
                    // Generate a Spec map for the enum field
                    val enumType = fieldDescriptor.enumType
                    val enumSymbols = enumType.values.map { it.name }
                   

 s/enum enumSymbols.toSet()
                }
                FieldDescriptor.JavaType.MESSAGE -> {
                    // Generate a Spec map for the nested message field
                    generateMessageSpec(fieldDescriptor.messageType)
                }
                else -> throw IllegalArgumentException("Unsupported field type: $fieldType")
            }
        }
    }
}

fun Builder.setField(fieldDescriptor: FieldDescriptor, value: Any) {
    when {
        fieldDescriptor.isMapField -> {
            // Convert the Spec map to a dynamic message for map fields
            val keyDescriptor = fieldDescriptor.messageType.findFieldByNumber(1)
            val valueDescriptor = fieldDescriptor.messageType.findFieldByNumber(2)

            @Suppress("UNCHECKED_CAST")
            value as Map<Any, Any>

            for ((key, v) in value) {
                val entryBuilder = newBuilderForField(keyDescriptor)
                        .setField(keyDescriptor, key)
                        .setField(valueDescriptor, v)
                addRepeatedField(fieldDescriptor, entryBuilder.build())
            }
        }
        fieldDescriptor.isRepeated -> {
            // Convert the Spec list to a dynamic message for repeated fields
            @Suppress("UNCHECKED_CAST")
            value as List<Any>

            for (v in value) {
                val elementBuilder = newBuilderForField(fieldDescriptor)
                        .setField(fieldDescriptor.messageType.findFieldByNumber(1), v)
                addRepeatedField(fieldDescriptor, elementBuilder.build())
            }
        }
        else -> setField(fieldDescriptor, value)
    }
}

fun DynamicMessage.toSpecString(): String {
    val jsonFormat = JsonFormat.printer()
    val json = jsonFormat.print(this)
    return json.replace("\"", "")
}
